resource "libvirt_cloudinit_disk" "commoninit" {
  count     = length(local.expanded_machines)
  name      = "commoninit.iso"
  pool      = libvirt_pool.machine[count.index].name
  user_data = data.template_file.user_data.rendered
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
}
