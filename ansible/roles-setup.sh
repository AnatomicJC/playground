#!/usr/bin/env python
import yaml
import subprocess
import os
from concurrent.futures import ThreadPoolExecutor
from tempfile import NamedTemporaryFile
import hashlib

script_directory = os.path.dirname(os.path.realpath(__file__))

roles_directory = os.path.join(script_directory, "external_roles")

with open(os.path.join(script_directory, 'requirements.yml'), 'r') as file:
    roles = yaml.safe_load(file)

def install_role(role):
    with NamedTemporaryFile('w', delete=False, suffix='.yml') as temp_file:
        yaml.dump([role], temp_file)
        temp_file_path = temp_file.name
    cmd = ['ansible-galaxy', 'role', 'install', '-r', temp_file_path, '-p', roles_directory, '--force']
    subprocess.run(cmd, check=True)
    os.remove(temp_file_path)
    galaxy_info_path = os.path.join(roles_directory, role['name'], 'meta', '.galaxy_install_info')
    if os.path.exists(galaxy_info_path):
        os.remove(galaxy_info_path)

def generate_checksums():
    checksums = {}
    for root, dirs, files in os.walk(roles_directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            relative_path = os.path.relpath(filepath, start=script_directory)
            with open(filepath, 'rb') as file:
                file_hash = hashlib.sha256()
                while chunk := file.read(4096):
                    file_hash.update(chunk)
                checksums[relative_path] = file_hash.hexdigest()
    
    sorted_checksums = sorted(checksums.items(), key=lambda x: x[0])
    
    with open(os.path.join(script_directory, 'external_roles_sha256sum.txt'), 'w') as checksum_file:
        for path, checksum in sorted_checksums:
            relative_checksum_path = os.path.relpath(path, start="external_roles")
            checksum_file.write(f'{relative_checksum_path}: {checksum}\n')

if not os.path.exists(roles_directory):
    os.makedirs(roles_directory)

num_cpus = os.cpu_count()

with ThreadPoolExecutor(max_workers=num_cpus) as executor:
    executor.map(install_role, roles)

generate_checksums()


