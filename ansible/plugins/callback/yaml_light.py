from ansible_collections.community.general.plugins.callback.yaml import CallbackModule as CallbackModule_default

DOCUMENTATION = """
    author: Jean-Christophe Vassort
    name: yaml_light
    type: stdout
    short_description: Remove ugly json output from ansible-playbooks output
    description:
        Remove ugly json output from ansible-playbooks output
    extends_documentation_fragment:
      - default_callback
    requirements:
      - set as stdout in configuration
"""

class CallbackModule(CallbackModule_default):
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = "stdout"
    CALLBACK_NAME = "yaml_light"

    def __init__(self):
        super(CallbackModule, self).__init__()

    def v2_runner_on_start(self, host, task):
        return True

    def _get_item_label(self, result):
        """retrieves the value to be displayed as a label for an item entry from a result object"""
        if result.get("_ansible_no_log", False):
            item = "(censored due to no_log)"
        else:
            item = result.get("_ansible_item_label", result.get("item"))
        if not isinstance(item, dict):
            return item
        return #item

# In [default] section of ansible.cfg

# callback_plugins = plugins/callback
# stdout_callback = yaml_light

# Use the stdout_callback when running ad-hoc commands.
# bin_ansible_callbacks = true
