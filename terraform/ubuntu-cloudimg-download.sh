#!/bin/sh

SCRIPT_DIR=$(cd -- "$(dirname -- "$0")" && pwd -P)
cd "${SCRIPT_DIR}"

mkdir -p ./downloads/
curl --progress-bar -L https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img -o ./downloads/ubuntu-22.04-server-cloudimg-amd64.img

qemu-img resize ./downloads/ubuntu-22.04-server-cloudimg-amd64.img +8G
