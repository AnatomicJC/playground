#!/bin/sh

set -eu

_wg () {
  docker run -i --rm --entrypoint /usr/bin/wg linuxserver/wireguard "${1}"
}

PRIVATE=$(_wg genkey)
echo "Private: ${PRIVATE}"
echo "Public: $(echo "${PRIVATE}" | _wg pubkey)"
