#!/bin/sh

SCRIPT_DIR=$(cd -- "$(dirname -- "$0")" && pwd -P)
cd "${SCRIPT_DIR}"

mkdir -p ./downloads/
curl --progress-bar -L https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2 -o ./downloads/debian-12-generic-amd64.qcow2

qemu-img resize ./downloads/debian-12-generic-amd64.qcow2 +8G
