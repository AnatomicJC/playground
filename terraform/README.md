# Terraform Memo

## Overview

There is 3 files defined:

* machines.tf contains the machines to be created
* network.tf contains the network definition
* variables.tf contains variables

## Provisioning

### Full

To provision the whole infrastructure:

```
terraform apply -auto-approve
```

To destroy the whole infrastructure:

```
terraform destroy -auto-approve
```

### Partial

Use case: you want to provision only kafka machines to work on kafka

The mandatory part is to provision the network and volumes parts:

```
terraform apply -auto-approve -target=module.network_luxapps -target=libvirt_volume.machine
```

Then you can create Kafka machines:

```
terraform apply -auto-approve -target=module.machine_kafka
```

If you want to add the infra machine:

```
terraform apply -auto-approve -target=module.machine_infra
```

You can also create the network / volume / kafka in one command:

```
terraform apply -auto-approve -target=module.network_luxapps -target=libvirt_volume.machine -target=module.machine_kafka
```
