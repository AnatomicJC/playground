#!/bin/sh

set -eu

rm -rf /tmp/ansible-facts

export ANSIBLE_HOST_KEY_CHECKING=False

ansible-playbook playbooks/base.yml -l proxmox,open_web_vpn_proxmox
ansible-playbook playbooks/vpn.yml -l proxmox,open_web_vpn_proxmox
