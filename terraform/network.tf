resource "libvirt_network" "machine" {
  autostart = true
  name      = var.network_name
  mode      = "nat"
  #domain    = "k8s.local"
  # IPv6 ULA address: https://tools.ietf.org/html/rfc4193
  addresses = ["${var.network}.0/24", "fd4a:fc40:8cfb::/64"]

  dhcp {
    enabled = false
  }

  bridge = "${var.network_name}-br"
  dns {
    enabled    = true
    local_only = true
  }
}
