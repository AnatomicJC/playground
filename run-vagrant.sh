#!/usr/bin/env bash

vagrant_cmd="vagrant$(if command -v vagrant.exe >/dev/null; then echo '.exe'; fi)"
export vagrant_cmd

RUNTIME_VAGRANT_CWD="vagrant/boxes"

export RUNTIME_VAGRANT_CWD

create_runtime_box()  {
  declare env="$1"

  log "[Start] Create runtime box for '$env'" "\x1B[1;4m"
  export VAGRANT_CWD="$RUNTIME_VAGRANT_CWD"
  export VAGRANT_STACK="$env"
  $vagrant_cmd box update
  $vagrant_cmd up --no-provision && $vagrant_cmd provision
  log "[End] Create runtime box" "\x1B[1;4m"
}

stop_runtime_box() {
  declare env="$1"

  log "[Start] Stop runtime box for '$env'" "\x1B[1;4m"
  export VAGRANT_CWD="$RUNTIME_VAGRANT_CWD"
  export VAGRANT_STACK="$env"
  $vagrant_cmd halt -f
  log "[End] Stop runtime box" "\x1B[1;4m"
}

destroy_runtime_box() {
  declare env="$1"

  log "[Start] Destroy runtime box for '$env'" "\x1B[1;4m"
  export VAGRANT_CWD="$RUNTIME_VAGRANT_CWD"
  export VAGRANT_STACK="$env"
  $vagrant_cmd destroy -f
  log "[End] Destroy runtime box" "\x1B[1;4m"
}

reload_runtime_box() {
  declare env="$1"

  log "[Start] Reload runtime for '$env'" "\x1B[1;4m"
  export VAGRANT_CWD="$RUNTIME_VAGRANT_CWD"
  export VAGRANT_STACK="$env"
  $vagrant_cmd up --no-provision
}


select_environment() {
  declare function="$1"
  clear

  ENV_MENU_DESCRIPTION="Choose one of the following environments:"
  ENV_OPTS=()

  echo -e "\x1B[1;4m$ENV_MENU_DESCRIPTION\x1B[0m"
  echo -e ""

  keys=($(awk -F= '{print $1}' vagrant/stack.ini))
  values=($(awk -F= '{print $2}' vagrant/stack.ini))
  stack_size=${#keys[@]}

  for ((i=0; i<$stack_size; i++)); do
    key="${keys[$i]}"
    value="${values[$i]}"
    ENV_OPTS+=("$key")
    echo -e "\x1B[1m$key\x1B[0m: $value"
  done

  select ENV_SELECTED in "${ENV_OPTS[@]}" "quit"; do
    if [[ $ENV_SELECTED == "all" ]]; then
      for env in "${ENV_OPTS[@]}"; do
        if [[ $env != "$ENV_SELECTED" ]];then
          ${function} "$env";
        fi
      done
    else
      ${function} "$ENV_SELECTED";exit;
    fi
  done
}

log() {
  declare message="$1" format="$2"
  printf "\x1B[1m[%-30s]\x1B[0m \x1B[31m%-10s\x1B[0m %-4s (\x1B[31m%-15s\x1B[0m) $format%s\x1B[0m\n" "$(date)" "${BASH_SOURCE}" "${BASH_LINENO[0]}" "${FUNCNAME[1]}" "$message"
}

main() {
  if [ -z "$VAGRANT_DEFAULT_PROVIDER" ]; then
    log "'VAGRANT_DEFAULT_PROVIDER' is not defined" >&2
    exit 1
  fi

  TASK_MENU_DESCRIPTION="Choose one of the following tasks:"
  TASK_OPTS=("start" "reload" "stop" "destroy")

  printf "\x1B[1;4m%s\x1B[0m\n" "$TASK_MENU_DESCRIPTION"
  printf "\n"
  printf "\x1B[1mstart\x1B[0m: Start the runtime box\n"
  printf "\x1B[1mreload\x1B[0m: Reload the runtime box\n"
  printf "\x1B[1mstop\x1B[0m: Stop the runtime box\n"
  printf "\x1B[1mDestroy\x1B[0m: Destroy the runtime box\n"
  printf "\n"

  select TASK_SELECTED in "${TASK_OPTS[@]}" "quit"; do
    case "$TASK_SELECTED" in
    'start') select_environment "create_runtime_box";exit;;
    'reload') select_environment "reload_runtime_box";exit;;
    'stop') select_environment "stop_runtime_box";exit;;
    'destroy') select_environment "destroy_runtime_box";exit;;
    'quit') exit;;
    *) echo "Invalid option. Try another one.";continue;;
    esac
    break
  done
}

[[ "$0" == "$BASH_SOURCE" ]] && main "$@"
