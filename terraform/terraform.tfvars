machines = [
  {
    machine_name  = "open-web"
    memory        = 2048
    cpu           = 2
    network_start = 10
    instances     = 3
    source        = "./downloads/debian-12-generic-amd64.qcow2"
  },
]
