locals {
  expanded_machines = flatten([
    for idx, machine in var.machines : [
      for i in range(machine.instances) : {
        machine_name  = "${machine.machine_name}-${i + 1}"
        memory        = machine.memory
        cpu           = machine.cpu
        source        = machine.source
        network_start = machine.network_start + i
      }
    ]
  ])
}

resource "libvirt_pool" "machine" {
  count = length(local.expanded_machines)
  name  = "${local.expanded_machines[count.index].machine_name}-pool"
  type  = "dir"
  path  = "/libvirt_images/${local.expanded_machines[count.index].machine_name}-pool/"
}

resource "libvirt_volume" "machine" {
  count  = length(local.expanded_machines)
  name   = "${local.expanded_machines[count.index].machine_name}.qcow2"
  pool   = libvirt_pool.machine[count.index].name
  source = local.expanded_machines[count.index].source
  format = "qcow2"
}

resource "libvirt_domain" "machine" {
  count     = length(local.expanded_machines)
  name      = local.expanded_machines[count.index].machine_name
  memory    = local.expanded_machines[count.index].memory
  vcpu      = local.expanded_machines[count.index].cpu
  cloudinit = libvirt_cloudinit_disk.commoninit[count.index].id
  network_interface {
    network_name   = var.network_name
    hostname       = local.expanded_machines[count.index].machine_name
    addresses      = ["${var.network}.${local.expanded_machines[count.index].network_start}"]
    wait_for_lease = false
  }
  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }
  disk {
    volume_id = libvirt_volume.machine[count.index].id
  }
  cpu {
    mode = "host-passthrough"
  }
  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}