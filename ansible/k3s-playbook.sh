#!/bin/sh

set -eu

rm -rf /tmp/ansible-facts

export ANSIBLE_HOST_KEY_CHECKING=False

ansible-playbook playbooks/base.yml -l k3s,open_web_vpn_1,open_web_vpn_2
ansible-playbook playbooks/vpn.yml -l k3s,open_web_vpn_1,open_web_vpn_2
ansible-playbook playbooks/k3s.yml -l k3s,open_web_vpn_1,open_web_vpn_2
