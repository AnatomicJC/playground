#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_MODULES_PATH=$(python -c "from os.path import join; from ansible import __path__; print(join(__path__[0], 'modules'))")

while IFS= read -r -d '' MODULE
do
  CURRENT_MODULE=$(grep "^module:" "${MODULE}" | awk '{print $2}' || true)
  if [ "${CURRENT_MODULE}" = "systemd_service" ]; then CURRENT_MODULE="systemd"; fi
  if [ "${CURRENT_MODULE}" = "group" ]
  then
    true
  elif [ -n "${CURRENT_MODULE}" ]
  then
    echo Search / Replace for "$CURRENT_MODULE"
    find . -type f -name "*\.y*ml" -exec sed -i "s/\(^[-[:blank:]]*\)\(${CURRENT_MODULE}:\)/\1ansible.builtin.\2/g" {} \;
  fi
done < <(find "${ANSIBLE_MODULES_PATH}" -maxdepth 1 -type f -print0)
