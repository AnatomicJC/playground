#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "$0")" && pwd -P)
cd "${SCRIPT_DIR}"

if [ -n "${VIRTUAL_ENV+x}" ]; then
  echo "Ansible virtualenv must be deactivated before running this script"
  echo "Please run deactivate command and try again"
  exit 1
fi

SED="sed"
POETRY_VENV_HOME=".cache"

if [ "$(uname)" = "Darwin" ]
then
  SED="gsed"
  POETRY_VENV_HOME="Library/Caches"
fi

# Create SSH socket path
mkdir -p ~/.ssh/sockets

# Remove old venv
#sudo rm -rf "${HOME}"/.cache/pypoetry/virtualenvs/ansible*

## Setup poetry
curl -sSL https://install.python-poetry.org | python3 -

## Setup ansible
${HOME}/.local/bin/poetry env use python3
${HOME}/.local/bin/poetry install --no-root

ANSIBLE_VENV="${HOME}/${POETRY_VENV_HOME}/pypoetry/virtualenvs/$(${HOME}/.local/bin/poetry env list | awk '{print $1}')"
export ANSIBLE_VENV

# ${SED} -i "s/^ANSIBLE_VERSION_MAX = (2, 13).*/ANSIBLE_VERSION_MAX = (2, 16)/g" \
#   "$(${HOME}/.local/bin/poetry run python -c "import sys; print(sys.path.pop())")"/ansible_mitogen/loaders.py

cat <<EOF | tee ansible-activate.txt

==============

Ansible is configured, add these 3 lines to your .bashrc:

. $(echo ${ANSIBLE_VENV})/bin/activate
export ANSIBLE_STRATEGY_PLUGINS=\$(python3 -c "import sys; print(sys.path.pop())")/ansible_mitogen/plugins/strategy
export ANSIBLE_STRATEGY=mitogen_linear
EOF

