variable "network" {
  type    = string
  default = "10.57.0"
}

variable "network_name" {
  type    = string
  default = "openweb"
}

# For image source, you can use local downloaded images:
# "./downloads/debian-12-generic-amd64.qcow2"
# "./downloads/ubuntu-22.04-server-cloudimg-amd64.img"
# 
# Or remote ones:
# 
# https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img
# https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2

variable "machines" {
  type = list(object({
    machine_name  = string
    memory        = number
    cpu           = number
    network_start = number
    instances     = number
    source        = string
  }))
  default = [
    {
      machine_name  = "open-web"
      memory        = 2048
      cpu           = 2
      network_start = 10
      instances     = 3
      source        = "./downloads/debian-12-generic-amd64.qcow2"
    },
  ]
}